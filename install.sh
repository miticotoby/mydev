#!/bin/bash

cd $(dirname $0)
MYPATH=$(pwd)
git submodule update --init --recursive

cd ${HOME}
ln -is ${MYPATH}/user/.bash_aliases
ln -is ${MYPATH}/user/.gitconfig
ln -is ${MYPATH}/user/.git-prompt-colors.sh
ln -is ${MYPATH}/user/.tmux.conf

cd ${HOME}/.config
ln -is ${MYPATH}/user/.config/bash-git-prompt
ln -is ${MYPATH}/user/.config/nvim
ln -is ${MYPATH}/user/.config/containers


mkdir -p ${HOME}/.local/share/nvim/ || /bin/true
ln -is ${HOME}/.config/nvim/pack ${HOME}/.local/share/nvim/pack


if ! command -v nvim >/dev/null; then
	echo "installing neovim..."
       sudo apt-get install neovim
fi

cd ${MYPATH}/user/.config/nvim/pack/myplugins/telescope-fzy-native.nvim/deps/fzy-lua-native
if [ ! -f "static/libfzy-linux-x86_64.so" ]; then
	echo "compiling helpers..."
       sudo apt-get install make gcc
	make
fi
