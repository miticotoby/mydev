FROM debian:bookworm
#FROM debian:bullseye
#FROM debian:buster

ENV GO_VER=1.21.4
ENV NODE_VER=21.2.0
ENV RUBY_VER=3.2.2
ENV RBENV_VER=1.2.0
ENV RUBY_BUILD_VER=20230717
#ENV SWAGGER_VER=0.26.1
#ENV PROTOBUF_VER=3.19.1

ENV USER=root
ENV HOME=/root

ENV BIN_PATH=$HOME/bin
ENV DEV_PATH=$HOME/dev
ENV RUSTUP_HOME=${DEV_PATH}/rust/rustup
ENV CARGO_HOME=${DEV_PATH}/rust/cargo

## os
RUN apt-get update
## install tools
RUN apt-get -y install git python3-pip python3-venv build-essential sqlite3 curl vim less tmux unzip libssl-dev fonts-powerline neovim telnet netcat-openbsd jq lua-nvim iproute2 man ripgrep fd-find sipcalc iputils-ping libyaml-dev rsync
## for buster
RUN . /etc/os-release; if test "${VERSION_CODENAME}" = "buster"; then apt-get -y install zlib1g-dev; fi
RUN apt-get clean && rm -rf /var/lib/apt/*

## run as regular user: problem is bind mounts later
#RUN useradd -m $USER
#USER $USER

RUN mkdir -p $DEV_PATH
RUN mkdir -p $BIN_PATH

## golang
RUN curl -L https://golang.org/dl/go$GO_VER.linux-amd64.tar.gz | tar xzf - -C $DEV_PATH \
	&& ln -s $DEV_PATH/go/bin/go $BIN_PATH/go \
	&& ln -s $DEV_PATH/go/bin/gofmt $BIN_PATH/gofmt

### protobuf
#RUN curl -Lo /tmp/protoc.zip https://github.com/protocolbuffers/protobuf/releases/download/v$PROTOBUF_VER/protoc-$PROTOBUF_VER-linux-x86_64.zip \
#	&& unzip /tmp/protoc.zip -d /tmp \
#	&& mv /tmp/bin/protoc $BIN_PATH \
#	&& mv /tmp/include/* /usr/local/include/ \
#	&& rm /tmp/protoc.zip /tmp/readme.txt \
#	&& rmdir /tmp/bin /tmp/include
#
### swagger
#RUN curl -L -o $BIN_PATH/swagger 'https://github.com/go-swagger/go-swagger/releases/download/v$SWAGGER_VER/swagger_linux_amd64' \
#	&& chmod 755 $BIN_PATH/swagger

## node
RUN curl -L https://nodejs.org/dist/v$NODE_VER/node-v$NODE_VER-linux-x64.tar.xz | tar xJf - -C $DEV_PATH \
	&& for i in node npm npx; do ln -s $DEV_PATH/node-v$NODE_VER-linux-x64/bin/$i $BIN_PATH/$i; done

## rust
RUN curl --proto =https --tlsv1.2 https://sh.rustup.rs -sSf -o /tmp/rust-install.sh
RUN bash /tmp/rust-install.sh -y --no-modify-path
RUN cd $BIN_PATH && for i in $DEV_PATH/rust/cargo/bin/*; do ln -s $i; done

## ruby
RUN curl -Lo /tmp/rbenv.zip https://github.com/rbenv/rbenv/archive/v$RBENV_VER.zip \
	&& unzip /tmp/rbenv.zip -d $DEV_PATH \
	&& rm /tmp/rbenv.zip \
	&& ln -s $DEV_PATH/rbenv-$RBENV_VER $DEV_PATH/rbenv \
	&& ln -s $DEV_PATH/rbenv/bin/rbenv $BIN_PATH/rbenv
RUN mkdir $DEV_PATH/rbenv-$RBENV_VER/plugins \
	&& curl -Lo /tmp/rbbuild.zip https://github.com/rbenv/ruby-build/archive/v$RUBY_BUILD_VER.zip \
	&& unzip /tmp/rbbuild.zip -d $DEV_PATH/rbenv-$RBENV_VER/plugins \
	&& rm /tmp/rbbuild.zip \
	&& ln -s $DEV_PATH/rbenv-$RBENV_VER/plugins/ruby-build-20210119 $DEV_PATH/rbenv/plugins/ruby-build
RUN RBENV_ROOT=$DEV_PATH/rbenv RUBY_BUILD_ROOT=$DEV_PATH/rbenv/plugins/ruby-build/share/ruby-build $BIN_PATH/rbenv install $RUBY_VER

## golang helpers
RUN GO111MODULE=on GOPATH=/root/go GOROOT=/root/dev/go GOBIN=/root/bin /root/bin/go install github.com/mgechev/revive@latest
RUN GO111MODULE=on GOPATH=/root/go GOROOT=/root/dev/go GOBIN=/root/bin /root/bin/go install golang.org/x/tools/cmd/guru@master
RUN GO111MODULE=on GOPATH=/root/go GOROOT=/root/dev/go GOBIN=/root/bin /root/bin/go install golang.org/x/tools/gopls@latest
RUN GO111MODULE=on GOPATH=/root/go GOROOT=/root/dev/go GOBIN=/root/bin /root/bin/go install github.com/davidrjenni/reftools/cmd/fillstruct@master
RUN GO111MODULE=on GOPATH=/root/go GOROOT=/root/dev/go GOBIN=/root/bin /root/bin/go install github.com/rogpeppe/godef@latest
RUN GO111MODULE=on GOPATH=/root/go GOROOT=/root/dev/go GOBIN=/root/bin /root/bin/go install github.com/fatih/motion@latest
RUN GO111MODULE=on GOPATH=/root/go GOROOT=/root/dev/go GOBIN=/root/bin /root/bin/go install github.com/kisielk/errcheck@latest
RUN GO111MODULE=on GOPATH=/root/go GOROOT=/root/dev/go GOBIN=/root/bin /root/bin/go install github.com/go-delve/delve/cmd/dlv@latest
RUN GO111MODULE=on GOPATH=/root/go GOROOT=/root/dev/go GOBIN=/root/bin /root/bin/go install golang.org/x/tools/cmd/gorename@master
RUN GO111MODULE=on GOPATH=/root/go GOROOT=/root/dev/go GOBIN=/root/bin /root/bin/go install github.com/koron/iferr@master
RUN GO111MODULE=on GOPATH=/root/go GOROOT=/root/dev/go GOBIN=/root/bin /root/bin/go install github.com/jstemmer/gotags@master
RUN GO111MODULE=on GOPATH=/root/go GOROOT=/root/dev/go GOBIN=/root/bin /root/bin/go install github.com/josharian/impl@master
RUN GO111MODULE=on GOPATH=/root/go GOROOT=/root/dev/go GOBIN=/root/bin /root/bin/go install golang.org/x/tools/cmd/goimports@master
RUN GO111MODULE=on GOPATH=/root/go GOROOT=/root/dev/go GOBIN=/root/bin /root/bin/go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest
RUN GO111MODULE=on GOPATH=/root/go GOROOT=/root/dev/go GOBIN=/root/bin /root/bin/go install github.com/fatih/gomodifytags@latest
RUN GO111MODULE=on GOPATH=/root/go GOROOT=/root/dev/go GOBIN=/root/bin /root/bin/go install honnef.co/go/tools/cmd/keyify@master
RUN GO111MODULE=on GOPATH=/root/go GOROOT=/root/dev/go GOBIN=/root/bin /root/bin/go install honnef.co/go/tools/cmd/staticcheck@latest
RUN GO111MODULE=on GOPATH=/root/go GOROOT=/root/dev/go GOBIN=/root/bin /root/bin/go install github.com/klauspost/asmfmt/cmd/asmfmt@latest

## customisation
RUN cp /etc/skel/.bashrc $HOME
COPY --chown=$USER:$USER user $HOME
RUN cd $HOME/.config/nvim/pack/myplugins/telescope-fzy-native.nvim/deps/fzy-lua-native && make
RUN mkdir $HOME/.config/nvim/pack/myide/start && cd $HOME/.config/nvim/pack/myide/start && for i in ../*; do ln -s $i; done && rm start

## shell
WORKDIR /data
CMD ["/bin/bash", "--login"]
