let mapleader=","
set mouse=
"set paste	"this for some reason aint' working
set noswapfile
"autocmd BufWritePre * :%s/\s\+$//e "strip trailing white space
set nrformats=hex
set nocompatible
set hidden
syntax on
filetype plugin indent on
set noerrorbells
set nobackup
set encoding=utf-8
"set autowrite
set autoread
set laststatus=2
set backspace=indent,eol,start
set clipboard=unnamed
" let $PATH = "/usr/local/bin:".$PATH
nnoremap <silent><expr> <Leader>h (&hls && v:hlsearch ? ':nohls' : ':set hls')."\n"


" short cut to yank to x register. this allows copy and paste between
" completely isolated vim sessions
:vnoremap <leader>y "xy<CR>:wviminfo! ~/.viminfo<CR>
:nnoremap <leader>p :rviminfo! ~/.viminfo<CR>"xp<CR>

" quick resize of split screen
nnoremap <silent> <Leader>> :exe "vertical resize +5"<CR>
nnoremap <silent> <Leader>< :exe "vertical resize -5"<CR>

"let i = 1
"while i <= 9
"    execute 'nnoremap <Leader>' . i . ' :' . i . 'wincmd w<CR>'
"    let i = i + 1
"endwhile
"function! WindowNumber()
"    let str=tabpagewinnr(tabpagenr())
"    return str
"endfunction
"set statusline=win:%{WindowNumber()}
"set statusline+=col:\ %c,
"set inccommand=split

"let g:titlecase_map_keys = 0

"autocmd BufNewFile,BufRead  *.lush :setfiletype lush
