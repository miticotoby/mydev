function! Bglight()
	colorscheme onehalflight
	let g:airline_theme='onehalflight'
endfunction

function! Bgdark()
	colorscheme onehalfdark
	let g:airline_theme='onehalfdark'
endfunction

call Bgdark()
