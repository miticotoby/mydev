set ruler

"nvim tweaks
set guicursor=i:block
set guicursor=r:block

set t_Co=256
set guifont=Hack:h18
"set listchars=tab:\.\ ,trail:·
"set listchars=tab:\ >\ ,trail:$,space:\xb7
set showbreak=↪\ 
set listchars=tab:→\ ,eol:↲,nbsp:␣,trail:•,extends:⟩,precedes:⟨
set list
set number
"set relativenumber
set numberwidth=5
set showcmd
set splitright
set splitbelow
set showmatch
set incsearch
set hlsearch
set ignorecase
set smartcase
set wildmenu
set lazyredraw          " redraw only when we need to.
set noshowmode          " you want this with airline, since it shows you your mode already
"set showmode           " you want this without theme. it'll show the mode


" shortcut to turn line numbering and git symbols on and off quickly
"nnoremap <leader>n :set number!<CR>:set relativenumber!<CR>:set list!<CR>:GitGutterToggle<CR>
nnoremap <leader>n :set number!<CR>:set list!<CR>:GitGutterToggle<CR>


set cursorline
"set cursorcolumn
set tabstop=4 softtabstop=0 noexpandtab shiftwidth=4 smarttab
" set tabstop=2
" set shiftwidth=2
" set expandtab
" set softtabstop=2
" set autoindent
" set smartindent
" set nowrap


"hi def Dim        cterm=none ctermbg=none  ctermfg=242

"function! s:DimInactiveWindow()
"    syntax region Dim start='' end='$$$end$$$'
"endfunction

"function! s:UndimActiveWindow()
"    ownsyntax
"endfunction

"autocmd WinEnter * call s:UndimActiveWindow()
"autocmd BufEnter * call s:UndimActiveWindow()
"autocmd WinLeave * call s:DimInactiveWindow()



"" ++++ color stuff ++++ ""
"set background=dark
"hi CursorLine     cterm=NONE ctermbg=232 ctermfg=none
"hi CursorColumn   cterm=NONE ctermbg=232 ctermfg=white guibg=darkgray guifg=white


"source $HOME/.config/nvim/colors/molokai.vim
"colorscheme molokai

"replaced by theme
"hi LineNr         cterm=none  ctermbg=none   ctermfg=243
"hi MatchParen     cterm=none  ctermbg=green  ctermfg=blue
"hi Search         cterm=none  ctermfg=white  ctermbg=244
