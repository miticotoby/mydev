" open files in a new tab
"nnoremap gf <C-W>gf
"vnoremap gf <C-W>gf

function! CloseAllBuffersButCurrent()
  let curr = bufnr("%")
  let last = bufnr("$")

  if curr > 1    | silent! execute "1,".(curr-1)."bd"     | endif
  if curr < last | silent! execute (curr+1).",".last."bd" | endif
endfunction

"nnoremap <leader>to :tabonly<cr>
"nnoremap <leader>tc :tabclose<cr>
"nnoremap <leader>tn :tabnext<cr>
"nnoremap <leader>tp :tabprevious<cr>
"nnoremap <leader>ls :tabs<cr>
"nnoremap <leader>ls :buffers<cr>
"nnoremap <leader>bb :bprev<cr>
"nnoremap <leader>bn :bnext<cr>
nnoremap <leader>bo :call CloseAllBuffersButCurrent()<CR>
nnoremap <leader>bc :bd<cr>
nnoremap <leader><Space> :bnext<cr>
nnoremap <leader><BS> :bprev<cr>

nnoremap <Leader>w <C-w><C-w>
nnoremap <Leader><Tab> <C-w><C-w>
nnoremap <Leader>v <C-w>v
nnoremap <Leader>q <C-w>q
nnoremap <Leader>. <C-w>10>
nnoremap <Leader>m <C-w>10<
nnoremap <Leader>+ <C-w>5+
nnoremap <Leader>- <C-w>5-

" quickfix window
nnoremap <Leader>co :copen<cr>
nnoremap <Leader>cc :ccl<cr>
nnoremap <Leader>cn :cn<cr>
nnoremap <Leader>cp :cp<cr>
