# This is an alternative approach. Single line in git repo.
override_git_prompt_colors() {
  GIT_PROMPT_THEME_NAME="Custom"

  GIT_PROMPT_PREFIX="[ "
  GIT_PROMPT_SUFFIX=" ]"
  GIT_PROMPT_SEPARATOR=" |"
  GIT_PROMPT_BRANCH="${BrightCyan}"
  GIT_PROMPT_STAGED=" ${Red}● ${ResetColor}"
  GIT_PROMPT_CONFLICTS=" ${Red}✖ ${ResetColor}"
  GIT_PROMPT_CHANGED=" ${Blue}✚ ${ResetColor}"
  GIT_PROMPT_UNTRACKED=" ${Cyan}…${ResetColor}"
  GIT_PROMPT_STASHED=" ${BoldBlue}⚑ ${ResetColor}"
  GIT_PROMPT_CLEAN=" ${BoldGreen}✔ ${ResetColor}"

  GIT_PROMPT_COMMAND_OK="${Green}✔ "
  GIT_PROMPT_COMMAND_FAIL="${Red}✘ "

  GIT_PROMPT_START_USER="\[\e]0;${debian_chroot:+($debian_chroot)}\w\a\] _LAST_COMMAND_INDICATOR_ ${White}${Time12a}${ResetColor} ${BrightGreen}\u@\h:${ResetColor}${BrightCyan}${PathShort}${ResetColor}"
  GIT_PROMPT_END_USER="${ResetColor} $ "
  #GIT_PROMPT_END_ROOT="${BoldRed} # "
  GIT_PROMPT_END_ROOT="${BoldRed} #${ResetColor} "
}

reload_git_prompt_colors "Custom"
