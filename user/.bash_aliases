alias ll='ls -lha'
#alias vi=vim
alias vi=nvim
alias netstat='ss'
#alias podman=docker
#alias mtr='mtr -bz'
alias gpg-list='gpg --list-keys --with-keygrip --keyid-format=long'
alias sshn='ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no'
alias root='ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i ~/.ssh/id_ed25519.mkinstall -l root '
alias rootscp='scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o User=root -i ~/.ssh/id_ed25519.mkinstall'
alias o=xdg-open
alias more='more -e'
alias pscg='ps -eo pid,stat,cputime,pmem,nice,user,cgroup:100,args -w --sort pid'
#alias mydev='podman run -it --rm -v ~/.ssh:/root/.ssh -v ${PWD}:/data --hostname mydev-${PWD##*/} --name mydev-${PWD##*/} mydev'

## for slacka - cli slack
#export AWKLIBPATH=/usr/local/lib/gawk

# env
export EDITOR=vi
export LESS=-R
#export PAGER="more -e"
export PAGER="less"
export CLICOLOR_FORCE=1

# go env
export GO111MODULE=on
export GOPRIVATE='gitlab.com/pincopallino/*'
export GOPATH=$HOME/go
export GOROOT=$HOME/dev/go
export GOBIN=$HOME/bin
#export GIT_ALLOW_PROTOCOL="ssh:git+ssh:https"

# ruby
export RBENV_ROOT=$HOME/dev/rbenv
export RUBY_BUILD_ROOT=$HOME/dev/rbenv/plugins/ruby-build/share/ruby-build
type -P rbenv >/dev/null 2>&1 && eval "$(rbenv init -)"
# default ruby version fallthrough
export RBENV_VERSION=${RUBY_VER:-3.0.2}

# rust
export CARGO_HOME="$HOME/dev/rust/cargo"
export RUSTUP_HOME="$HOME/dev/rust/rustup"

# set PATH so it includes user's private bin if it exists
# this is added usually by .profile, but not on all distros
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

## bash specific tweaks
if [ -n "$BASH_VERSION" ]; then
	if [ -d "$HOME/.config/bash-git-prompt" ]; then
		GIT_PROMPT_THEME="Custom"
		#GIT_PROMPT_ONLY_IN_REPO=1
		source $HOME/.config/bash-git-prompt/gitprompt.sh
	else
		## original git prompt provided by git package
		export PROMPT_COMMAND='__git_ps1 "\[\033[1;32m\]\u@\h:\[\033[0;0m\]\[\033[1;36m\]\w\[\033[0;0m\]" "\\\$ "'
		export GIT_PS1_SHOWDIRTYSTATE=true
		export GIT_PS1_SHOWSTASHSTATE=true
		export GIT_PS1_SHOWUNTRACKEDFILES=true
		export GIT_PS1_SHOWCOLORHINTS=true
		export GIT_PS1_SHOWUPSTREAM="verbose verbose"
		#export GIT_PS1_SHOWUPSTREAM="auto"
	fi
fi

## zsh specific tweaks
if [ -n "$ZSH_VERSION" ]; then
	## ctrl+u bash bahaviour
	bindkey "^U" backward-kill-line

	## autocomplete
	setopt noautomenu
	setopt nomenucomplete
	setopt interactivecomments
	unsetopt correct_all
	unsetopt correct
	unsetopt autocd

	POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS+=("time")
	# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
	#[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
fi

if [ -f ~/.bash_local ]; then
    . ~/.bash_local
fi
